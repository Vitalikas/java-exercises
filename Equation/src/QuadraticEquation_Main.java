import java.util.ArrayList;
import java.util.List;

public class QuadraticEquation_Main {

    public static void main(String[] args) {
        QuadraticEquation equation1 = new QuadraticEquation(1,  -2, -3);
        QuadraticEquation equation2 = new QuadraticEquation(-1,-2,15);
        QuadraticEquation equation3 = new QuadraticEquation(1,2,-3);
        QuadraticEquation equation4 = new QuadraticEquation(1,6,9);

        System.out.printf("Kvadratinė lygtis: %.2fx2 + %.2fx + %.2f = 0\n", equation1.getA(), equation1.getB(), equation1.getC());
        System.out.printf("Koeficientai: a = %.2f b = %.2f c = %.2f\n", equation1.getA(), equation1.getB(), equation1.getC());

        System.out.println("D = " + equation1.calcD());

        if (equation1.calcD() > 0) {
            System.out.println("D > 0, kvadratinė lygtis turi dvi šaknis:");
            System.out.println("x1 = " + equation1.calcX1());
            System.out.println("x2 = " + equation1.calcX2());
        } else if (equation1.calcD() == 0) {
            System.out.println("D = 0, kvadratinė lygtis turi vieną šaknį:");
            System.out.println("x = " + equation1.calcX());
        } else System.out.println("D < 0, nėra šaknų");

        double sumOfA = calcSumOfA(equation1, equation2, equation3, equation4);
        System.out.println("\nVisų paduodamų lygčių A koeficientų suma: " + sumOfA);

        List dOfObjs = calcDofObjs(equation1, equation2, equation3, equation4);
        List x1OfObjs = calcX1OfObjs(equation1, equation2, equation3, equation4);
        List x2OfObjs = calcX2OfObjs(equation1, equation2, equation3, equation4);
        List xOfObjs = calcXOfObjs(equation1, equation2, equation3, equation4);

        for (int i = 0; i < dOfObjs.size(); i++) {
            System.out.println("D[" + (i+1) + "] = " + dOfObjs.get(i));
            if ((double)dOfObjs.get(i) > 0) {
                for (int j = 0; j < x1OfObjs.size(); j++) {
                    System.out.println("x1[" + (j+1) + "] = " + x1OfObjs.get(j));
                }
                for (int k = 0; k < x2OfObjs.size(); k++) {
                    System.out.println("x2[" + (k+1) + "] = " + x2OfObjs.get(k));
                }
            } else if ((double)dOfObjs.get(i) == 0) {
                for (int l = 0; l < xOfObjs.size(); l++) {
                    System.out.println("x[" + (l+1) + "] = " + xOfObjs.get(l));
                }
            }
        }
        //for (Object d : dOfObjs) { // d - objektas
        //    double D = (double) d; // D - casted objektas
        //    System.out.println("D = " + D);
        //}
        //    if (D > 0) {
        //        for (Object x1 : x1OfObjs) {
        //            System.out.println("x1 = " + x1);
        //        }
        //        for (Object x2 : x2OfObjs) {
        //            System.out.println("x2 = " + x2);
        //        }
        //    } else if (D == 0) {
        //        for (Object x : xOfObjs) {
        //            System.out.println("x = " + x);
        //        }
        //    } else if (D < 0) {
        //        System.out.println("D < 0, nėra šaknų");
        //    }
    }

    public static double calcSumOfA(QuadraticEquation ... equations) {
        double sum = 0;
        for (QuadraticEquation equation : equations) {
            sum += equation.getA();
        } return sum;
    }

    public static List calcDofObjs(QuadraticEquation ... equations) {
        List list = new ArrayList();
        for (QuadraticEquation equation : equations) {
            list.add(Math.pow(equation.getB(), 2) - 4 * equation.getA() * equation.getC());
        } return list;
    }

    public static List calcX1OfObjs(QuadraticEquation ... equations) {
        List list = new ArrayList();
        for (QuadraticEquation equation : equations) {
            list.add((-equation.getB() + Math.sqrt(equation.calcD())) / (2 * equation.getA()));
        } return list;
    }

    public static List calcX2OfObjs(QuadraticEquation ... equations) {
        List list = new ArrayList();
        for (QuadraticEquation equation : equations) {
            list.add((-equation.getB() - Math.sqrt(equation.calcD())) / (2 * equation.getA()));
        } return list;
    }

    public static List calcXOfObjs(QuadraticEquation ... equations) {
        List list = new ArrayList();
        for (QuadraticEquation equation : equations) {
            list.add((-equation.getB()) / (2 * equation.getA()));
        } return list;
    }
}