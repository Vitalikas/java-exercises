public class QuadraticEquation {

    private double a;
    private double b;
    private double c;

    public QuadraticEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }

    // D is discriminant
    public double calcD() {
        double D = Math.pow(getB(), 2) - 4 * getA() * getC();
        return D;
    }

    public double calcX1() {
        double x1 = (-getB() + Math.sqrt(calcD())) / (2 * getA());
        return x1;
    }

    public double calcX2() {
        double x2 = (-getB() - Math.sqrt(calcD())) / (2 * getA());
        return x2;
    }

    public double calcX() {
        double x = (-getB()) / (2 * getA());
        return x;
    }
}