public class Run {

    Msg msg = new Msg();

    public void exe() {
        msg.welcomeMessage();
        msg.question();
        msg.farewellMessage();
    }

}