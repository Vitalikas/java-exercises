import java.io.IOException;

public class Msg {

    Cart cart = new Cart();

    public void welcomeMessage() {
        System.out.println("Sveiki atvykę į MAXIMA!");
    }

    public void farewellMessage() {
        System.out.println("\nDėkojame, kad apsilankėtė! Geros dienos!");
    }

    public void question() {
        System.out.println("\nKą norėtumėte atlikti?");
        System.out.println("\t[1] - rodyti parduotuvės asortimentą");
        System.out.println("\t[2] - įdėti prekę į krepšelį");
        System.out.println("\t[3] - pašalinti prekę iš krepšelio");
        System.out.println("\t[4] - rodyti krepšelį");
        System.out.println("\t[5] - susimokėti už prekes");
        System.out.print("\nĮveskite skaičių: ");

        String selection = ScannerIn.scanner.next();
        switch (selection) {
            case "1":
                cart.shopProducts.showPriceListAndNumberOfProducts();
                question();
                break;
            case "2":
                cart.shopProducts.showPriceListAndNumberOfProducts();
                cart.addToCart();
                question();
                break;
            case "3":
                cart.showCart();
                cart.removeFromCart();
                question();
                break;
            case "4":
                cart.showCart();
                question();
                break;
            case "5":
                try {
                    cart.checkCartBeforeWritingToRegistry();
                } catch (IOException e) {
                    System.out.println("AAA");
                }
                break;
            default:
                System.out.println("\nNeteisingas pasirinkimas. Bandykite dar kartą.");
                question();
                break;
        }
    }

}