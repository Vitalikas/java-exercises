import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Cart {

    int VAT = 21;

    //iskvieciame klase, kurioje saugomi visi produktai(objektai), kurie sudeti i masyva
    ShopProducts shopProducts = new ShopProducts();

    //sukuriame prekiu krepseli, i kuri desime prekes
    List<Object> cart = new ArrayList<Object>();

    //krepselio suma
    List<Double> cartPrice = new ArrayList<Double>();

    public void addToCart() {
        boolean itemNotAddedToCart = true;
        System.out.print("Parašykite produktą: ");
        String product = ScannerIn.scanner.next();
//        for (int i = 0; i < shopProducts.products.size(); i++) {
//            if (shopProducts.products.get(i).getName().equalsIgnoreCase(product)) {
//                //add it to the cart (array)
//                cart.add(shopProducts.products.get(i).toString());
//                System.out.println("\n" + shopProducts.products.get(i).getName().toUpperCase() + " | KAINA: " + shopProducts.products.get(i).getPrice() + "€" + " į krepšelį įdėta sėkmingai");
//                cartPrice.add(shopProducts.products.get(i).getPrice());
//                itemNotAddedToCart = false;
//            }
//        }
        for (Product item : shopProducts.products) {
            if (item.getName().equalsIgnoreCase(product)) {
                cart.add(item);
                cartPrice.add(item.getPrice());
                System.out.println(item + " į krepšelį įdėta sėkmingai");
                itemNotAddedToCart = false;
            }
        }
        if (itemNotAddedToCart) {
            System.out.println("Atsiprašome, norimos prekės nėra sąraše. Bandykite dar kartą");
            addToCart();
        }
    }

    public void showCart() {
        if (cart.size() > 0) {
            System.out.println("\n####################################################");
            System.out.println("Krepšelyje yra prekių: " + cart.size());
//            for (int i = 0; i < cart.size(); i++) {
//                System.out.println("\t" + (i + 1) + ". " + cart.get(i).toUpperCase());
//            }
            for (Object item : cart) {
                int i = 1;
                System.out.println("\t" + i + ". " + item);
                i++;
            }
            System.out.println("----------------------------------------------------");
            System.out.printf("Nuolaida: %.2f€", calc30percentWeekendDiscount(calcCartSum()));
            System.out.println("\n-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -");
            System.out.printf("PVM = %d%s\tbe PVM %.2f€\tPVM %.2f€\tsu PVM %.2f€\n", VAT, "%", calcCartPriceWithDiscount() / 1.21, (calcCartPriceWithDiscount() - calcCartPriceWithDiscount() / 1.21), calcCartPriceWithDiscount());
            System.out.println("----------------------------------------------------");
            System.out.printf("MOKĖTI: %.2f€", calcCartPriceWithDiscount());
            System.out.println("\n\n\n" + addDateAndTimeToBill());
            System.out.println("####################################################");
        } else {
            System.out.println("Jūsų krepšelis tuščias");
        }
    }

    public void removeFromCart() {
        if (cart.size() > 0) {
            System.out.println("Kurią prekę norite pašalinti?");
            System.out.print("Įveskite prekės numerį: ");
            errorCatchStringInstedOfInt();
        } else {
            System.out.println("Nėra ką šalinti.");
        }
    }

    public void errorCatchStringInstedOfInt() {
        try {
            if (ScannerIn.scanner.hasNextInt()) {
                int input = ScannerIn.scanner.nextInt();
                if (input <= cart.size()) {
                    String selectedProduct = (String) cart.get(input - 1);
                    cart.remove(input - 1);
                    System.out.println("Jus sėkmingai pašalinote " + selectedProduct.toUpperCase());
                    cartPrice.remove(input - 1);
                } else {
                    System.out.println("Tokios prekės numerio nėra");
                }
            } else {
                System.out.println("Jus įvedėt: " + ScannerIn.scanner.next() + ". Veskite numerį!");
                removeFromCart();
            }
        }
        catch (InputMismatchException exception) {
            System.out.println("Nėra ką šalinti.");
            removeFromCart();
        }
    }

    public double calcCartSum() {
        double sum = 0;
        for (double i : cartPrice) {
            sum += i;
        }
        return sum;
    }

    public boolean checkIfWednesdaySaturdaySunday() {
        boolean isWednesdaySaturdaySunday = false;
        Calendar calendar = Calendar.getInstance();
        if ((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) || (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) || (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)) {
            isWednesdaySaturdaySunday = true;
        } return isWednesdaySaturdaySunday;
    }

    public double calc30percentWeekendDiscount(double cartTotalPrice) {
        double discount;
        if (checkIfWednesdaySaturdaySunday()) {
            discount = 30;
        } else {
            discount = 0;
        }
        return cartTotalPrice * discount / 100;
    }

    public double calcCartPriceWithDiscount() {
        return calcCartSum() - calc30percentWeekendDiscount(calcCartSum());
    }

    public String addDateAndTimeToBill() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime time = LocalDateTime.now();
        return dateTimeFormatter.format(time);
    }

    public double payForProducts() {
        System.out.println("Įveskite pinigų sumą:");
        Scanner input = new Scanner(System.in);
        double cash = input.nextDouble();
        return cash;
    }

    public void writeBilltoFile() throws IOException {
        PrintWriter printWriter = new PrintWriter(new FileWriter("src\\bill.txt", true));
        printWriter.print("############################################################");
        printWriter.println("\nPirkta prekių: " + cart.size());
        for (int i = 0; i < cart.size(); i++) {
            printWriter.println("\t" + (i + 1) + ". " + cart.get(i));
        }
        printWriter.println("------------------------------------------------------------");
        printWriter.printf("Nuolaida: %.2f€", calc30percentWeekendDiscount(calcCartSum()));
        printWriter.println("\n-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -");
        printWriter.printf("PVM = %d%s\tbe PVM %.2f€\tPVM %.2f€\tsu PVM %.2f€\n", VAT, "%", calcCartPriceWithDiscount() / 1.21,
                (calcCartPriceWithDiscount() - calcCartPriceWithDiscount() / 1.21), calcCartPriceWithDiscount());
        printWriter.println("------------------------------------------------------------");
        printWriter.printf("MOKĖTI: %.2f€", calcCartPriceWithDiscount());
//        printWriter.printf("\nMOKĖTA GRYNAIS: %.2f€", 10);
//        printWriter.printf("\nGRĄŽĄ: %.2f€", 10 - calcCartPriceWithDiscount());
        printWriter.println("\n\n\n" + addDateAndTimeToBill());
        printWriter.println("############################################################");
        printWriter.close();
    }

    public void checkCartBeforeWritingToRegistry() throws IOException{
        if (cart.size() > 0) {
            if (payForProducts() >= calcCartPriceWithDiscount()) {
                writeBilltoFile();
            } else {
                System.out.println("Nepakanka grynųjų apmokėti už prekes");
                checkCartBeforeWritingToRegistry();
            }
        }
    }

}