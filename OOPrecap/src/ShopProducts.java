import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ShopProducts {

    private static Scanner read;

    List<Product> products = importProducts();

    //List<Product> products = List.of(
    //        new Product("pienas", 1.15),
    //        new Product("duona", 0.80),
    //        new Product("sūris", 1.30),
    //        new Product("bulvės", 0.70),
    //        new Product("arbata", 2.10)
    //);

//    public void showPriceListOfProducts() {
//        for (int i = 0; i < products.size(); i++) {
//            System.out.println((i + 1) + ". " + products.get(i).getName().toUpperCase()  + " | kaina: " + products.get(i).getPrice() * products.get(i).getVat() + "€");
//        }
//    }

    public void showPriceListOfProducts() {
        int i = 1;
        for (Product product : products) {
            System.out.println(i + ". " + product);
            i++;
        }
    }

    public void showNumberOfProducts() {
        System.out.println("\nIš viso prekių parduotuvėje: " + products.size());
    }

    public void showPriceListAndNumberOfProducts() {
        showPriceListOfProducts();
        showNumberOfProducts();
    }

    public static void openFile() {
        try {
            read = new Scanner(new File("src\\warehouse.txt"));
        } catch (Exception e) {
            System.out.println("Failo nerasta");
        }
    }

    public static List<Product> importProducts() {
        List<Product> productsList = new ArrayList<Product>();
        openFile();
        read.nextLine();
        while (read.hasNext()) {
            String dataFromFile = read.nextLine();
            String[] valuesFromLine = dataFromFile.split(",");
            String name = valuesFromLine[0].strip();
            double price = Double.parseDouble(valuesFromLine[1]);
            int quantity = Integer.parseInt(valuesFromLine[2]);
            productsList.add(new Product(name, price, quantity));
        }
        read.close();
        return productsList;
    }

}